# traffic_classifier


## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)


## Name
Network Packet Classification with Neural Networks

## Description
The primary objective of our research is to develop a neural network-based system for packet classification and to enhance its performance with continual updates and modifications which form our streaming techniques. In network management, packet classification is an important function that allows efficient routing, security measures, and quality of service. As network traffic grows more complex and more dense, traditional methods often fail to cope with timely analysis [1]. This project uses neural networks to classify packets more accurately and efficiently, eliminating existing limitations.

This research is expected to lead to the development of a real-time packet classification system which will employ packet-filtration, quality of service, network monitoring, and resource optimization based on heuristics derived from this neural network system. By incorporating streaming techniques, the system will be able to adapt to dynamic network traffic and morphology of cyberattacks and protocol changes thus ensuring continuous and accurate classification [2]. The benefit of this classification engine provides data for an ever-changing digital environment, facilitating long-term maintenance of network security and performance equipment and services. By providing an open-ended model that allows the analysis to run the spectrum from metadata to full packet analysis, we give the greatest flexibility to the types of analysis that can be performed [3]. Moreover, the project aims to contribute insights into how neural networks and streaming can be used to classify network traffic to the machine learning community.

This project is anticipated to have significant implications for network management when it is successfully completed. To drive awareness and accessibility to the broader security and analysis community, it will be released as open-source code. As a result of enhanced packet classification, security threats will be detected more efficiently, bandwidth allocation will be optimized, and users will receive better service. In addition, the findings of this study may pave the way for future research in this field, exploring advanced neural network architectures and packet classification techniques.

## Support
Contact the authors for support. No warranty or representation of merchantability is implied.

## Contributing
Outside contributions to this project can be given after May 2024 through any of the Authors.

## Authors and acknowledgment
Tyler Franzen, David Loper, Murat Parlakisik

## License
The project is licensed under AGPL v3.0+

## Project status
As of April 2024, this project is under current development

## References
[1] “Deep Packet Inspection vs. Metadata Analysis of Network Detection & Response (NDR) Solutions,” The Hacker News. Accessed: Feb. 27, 2024. [Online]. Available: https://thehackernews.com/2022/11/deep-packet-inspection-vs-metadata.html 
[2] V. Chhabria, “Leveraging Data from Open-Source Intrusion Detection Systems for Enhancing Security of Systems”. 
[3] C. Fridgen, “Full packet analysis vs metadata analysis: which is more effective for network monitoring?,” Accedian. Accessed: Feb. 27, 2024. [Online]. Available: https://accedian.com/blog/full-packet-analysis-vs-metadata-analysis-which-is-more-effective/ 

